<?php namespace CMS;

use App\Page;
use HieuLe\Active\Facades\Active;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Schema;

class CMS
{

    public static function getStyleID()
    {
        $current = Route::currentRouteName();
        $baseURL = URL::to('/');

        if (is_null($current)) {
            $current = URL::current();
            $current = str_replace($baseURL, '', $current); // remove baseURL so we get the url
        }

        // Remove dots at the route if placed or slashes
        $current = str_replace(array('.', ',', '/'), ' ', $current);
        return $current;
    }

    /**
     * Returns defined logo ( Image etc. )
     * @return string
     */
    public static function getLogo()
    {
        return 'LOGO';
    }

    public static function getTitle()
    {
        return 'CMS';
    }

    public static function routeCustomPages()
    {
        if (Schema::hasTable('page')) {
            $page = \App\Http\Controllers\PagesController::getNavigationParent();

             // Loop through each page that is a parent sorted on weight
            foreach ($page as $p) {
                 // If page is active, use route, else disable
                if ($p->active) {
                     // If login has to be required, then enable Auth login (FB login )
                    $p->routeAuth('PagesController@pages');

                    // Route is set for parent but check whether it has children so we can group them under the url
                    if ($p->hasChildren()) {
                        // Take the prefix from the url of the parent
                        Route::group(['prefix' => $p->url], function () use ($p) {
                            foreach ($p->getChildren() as $child) {
                                // Same method for children, if active, then post  and if there is an auth enabled, then use it
                                if ($p->active) {
                                     ($child->auth) ?  Route::get($child->url, ['middleware' => 'auth', 'as' => $child->url, 'uses' => 'PagesController@pages']) : Route::get($child->url, ['as' => $child->url, 'uses' => 'PagesController@pages']);
                                }
                            }
                        }
                        );
                    }

                }

            }
        }
    }



    /** Before => Auth
     * Authentication routes, no authenticated user = no access
     * Used for the CMS
     **/
    public static function routesCMS()
    {
        Route::group(['before' => 'auth'], function () {

            Route::get('dashboard', ['middleware' => 'auth', 'as' => 'dashboard', 'uses' => 'PagesController@dashboard']);
            Route::get('users', ['middleware' => 'auth', 'as' => 'users', 'uses' => 'UserController@index']);
            Route::get('roles', ['middleware' => 'auth', 'as' => 'roles', 'uses' => 'UserController@roles']);
            Route::get('logout', ['middleware' => 'auth', 'uses' => 'Auth\AuthController@getLogout']);
            Route::get('profile', ['middleware' => 'auth', 'as' => 'profile', 'uses' => 'PagesController@profile']);

            Route::group(['prefix' => 'articles'], function () {
                Route::get('', ['middleware' => 'auth', 'as' => 'articles', 'uses' => 'ArticleController@index']);
                Route::get('createnew', ['middleware' => 'auth', 'as' => 'articles.create', 'uses' => 'ArticleController@create']);
                Route::get('show/{id}', ['middleware' => 'auth', 'as' => 'articles.show', 'uses' => 'ArticleController@show']);
                Route::get('edit/{id}', ['middleware' => 'auth', 'as' => 'articles.edit', 'uses' => 'ArticleController@edit']);
                Route::get('destroy/{id}', ['middleware' => 'auth', 'as' => 'articles.destroy', 'uses' => 'ArticleController@destroy']);
                Route::post('create', ['middleware' => 'auth', 'as' => 'articles.create', 'uses' => 'ArticleController@create']);
                Route::post('update/{id}', ['middleware' => 'auth', 'as' => 'articles.update', 'uses' => 'ArticleController@update']);
            });

            Route::group(['prefix' => 'pages'], function () {
                Route::get('', ['middleware' => 'auth', 'as' => 'pages', 'uses' => 'PagesController@listpages']);
                Route::get('createnew', ['middleware' => 'auth', 'as' => 'pages.createnew', 'uses' => 'PagesController@create']);
                Route::get('show/{id}', ['middleware' => 'auth', 'as' => 'page.show', 'uses' => 'PagesController@show']);
                Route::get('edit/{id}', ['middleware' => 'auth', 'as' => 'pages.edit', 'uses' => 'PagesController@edit']);
                Route::get('destroy/{id}', ['middleware' => 'auth', 'as' => 'pages.destroy', 'uses' => 'PagesController@destroy']);
                Route::post('create', ['middleware' => 'auth', 'as' => 'pages.create', 'uses' => 'PagesController@create']);
                Route::post('update/{id}', ['middleware' => 'auth', 'as' => 'pages.update', 'uses' => 'PagesController@update']);
            });

        });
    }

    public static function  routeLogin() {

    }

}
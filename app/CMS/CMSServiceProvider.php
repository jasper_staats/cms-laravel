<?php namespace CMS;

use Illuminate\Support\ServiceProvider;

/*
 * CMS
 * */
class CMSServiceProvider extends ServiceProvider{

    public function register()
    {
        $this->app->bind('CMS', 'CMS/Facades' );
    }
}
<?php

use CMS\Classes\Facades\CMS;

Route::when('*', 'auth.basic');
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@pages']);

// All groups that contain auth prefix
Route::group(['prefix' => 'auth'], function () {
    Route::get('login', 'PagesController@login');

    Route::group(['prefix' => 'facebook'], function () {
        Route::get('', ['as' => 'auth/facebook', 'uses' => 'Auth\AuthController@redirectToProvider']);
        Route::get('callback', ['as' => 'auth/facebook/callback', 'uses' => 'Auth\AuthController@handleProviderCallback']);
    });
});


// Route the customPages that are defined by the user
\CMS\CMS::routeCustomPages();

// All Dashboard routes
\CMS\CMS::routesCMS();


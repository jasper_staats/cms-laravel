<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;


class RoleController extends Controller
{
    public function create(Request $request){
        $method = $request->method();
        if ($request->isMethod('post')) {
            $Roles = new Roles();
            $Roles->function = Input::get('function');
            $Roles->save();
            return Redirect::route('users')->with('flash_success', 'Nieuwe rol [ '.$Roles->function.' aangemaakt');
        }
        else {
            Session::flash('snap_notice','Begin met het maken van een nieuwe rol');
            return view('pages.users.roles.create');
        }
    }

    public function edit($id){
        Session::flash('snap_notice','Begin met het het aanpassen van rollen');
        $role = Roles::findOrFail($id);
        return view('pages.users.role.edit',compact('role'));
    }

    public function update(Request $request, $id) {
        $Roles = Roles::findOrFail($id);
        $Roles->function = Input::get('function');
        $Roles->save();
        return Redirect::route('users')->with('flash_success', 'Rol aangepast!');
    }

    public function destroy($id){
        Roles::destroy($id);
        return Redirect::route('users')->with('flash_notice', 'Rol verwijderd');
    }
}

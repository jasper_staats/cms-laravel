<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller
{

    public static function getRecent($sort,$type) {
        $page = Page::where('url',$type)->get();

        $page_id = 1;
        foreach($page as $p) {
            $page_id = $p->id;
        }

        $articles  =  Article::orderBy('published_at',$sort)->where('page',$page_id)->get();
        return $articles;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('pages.articles.list',compact('articles'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $method = $request->method();
        if ($request->isMethod('post')) {
            $user = Auth::user();
            $Article = new Article();
            $Article->title = Input::get('title');
            $Article->body = Input::get('body');
            $Article->summary = Input::get('summary');
            $Article->published = (int)Input::get('published');
            $Article->published_at = Carbon::now();
            $Article->user_id = $user->id;
            $Article->save();
            return Redirect::route('articles')->with('flash_success', 'Nieuw artikel succesvol aangemaakt');
        }
        else {
            Session::flash('snap_notice','Begin met het maken van een artikel');
            return view('pages.articles.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);
        return view('pages.articles.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Session::flash('snap_notice','Begin met het het aanpassen');
        $article = Article::findOrFail($id);
        return view('pages.articles.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Article = Article::findOrFail($id);
        $user = Auth::user();
        $Article->title = Input::get('title');
        $Article->body = Input::get('body');
        $Article->summary = Input::get('summary');
        $Article->user_id = $user->id;
        $Article->save();
        return Redirect::route('articles')->with('flash_success', 'Nieuw artikel succesvol aangepast');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
        return Redirect::route('articles')->with('flash_notice', 'Artikel verwijderd');
    }
}

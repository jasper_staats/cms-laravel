<?php

namespace App\Http\Controllers;

use App\Page;
use CMS\CMS;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Article;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

/**
 * Pagescontroller for paging
 *
 */
class PagesController extends Controller
{


    public static function getNavigationParent(){
        $pages = Page::orderBy('weight','asc')->get();
        $pages_nav = [];

        foreach($pages as $page) {
            if(!$page->isChild()) {
                    $pages_nav[] = $page;
            }
        }

        return $pages_nav;
    }

    public function index()
    {
        $articles = Article::all();
        return view('index', compact('articles'));
    }

    public function pages(){
        $page = Route::currentRouteName();

        if($page == '')
        {
            $page = 'home';
        }

        $page = Page::where('url',$page)->get();

        $page_id = 1;
        foreach($page as $p) {  $page_id = $p->id; $page_type = $p->name; $page_url = $p->url;   }
        $articles = Article::where('page',$page_id)->get();
        return view('pages.custom.index',compact('articles','page_type','page_url'));
    }

    public function dashboard()
    {
        return view('pages.dashboard.index');
    }

    public function users()
    {
        return view('pages.users');
    }

    public function login()
    {
        return view('pages.login.index');
    }

    public function profile()
    {
        return Redirect::route('home')->with('snap_notice', 'Komt nog erbij!');
    }

    public function create(Request $request)
    {
        $method = $request->method();
        if ($request->isMethod('post')) {

            $Page = new Page();
            $Page->url = Input::get('url');
            $Page->name = Input::get('name');
            $Page->description = Input::get('description');
            $Page->active = Input::get('active');
            $Page->parent = Input::get('parent');
            $Page->deleteable = Input::get('deleteable');
            $Page->auth = Input::get('auth');
            $Page->active = Input::get('active');
            $Page->weight = Input::get('weight');
            $Page->save();

            if($Page->parent > 0) {
                $parent = Page::findOrFail($Page->parent);
                $parent->deleteable = 0;
                $parent->save();
            }

            return Redirect::route('dashboard')->with('flash_success', 'Nieuwe pagina succesvol aangemaakt');
        }
        else {
            Session::flash('snap_notice','Begin met het maken van een nieuwe pagina');
            return view('pages.create');
        }


    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Session::flash('snap_notice','Begin met het het aanpassen');
        $page = Page::findOrFail($id);
        return view('pages.edit',compact('page'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);

        if($page['deleteable']) {



            Page::destroy($id);

            /*
             * Update articles, if there are articles which page type , then change to null
             */
            $articles = Article::where('page',$page->id)->get();
            if($articles) {
                foreach($articles as $article) {
                    $article->page = 0;
                    $article->save();
                }
            }
            /*
             * Update parent , if there are no children, then allow it to be deleteable by the user
             */
            $parent = Page::find($page->parent);
            if($parent) {
                if (!$parent->hasChildren() && $parent->name != 'home') {
                    $parent->deleteable = 1;
                    $parent->save();
                }
            }
            return Redirect::route('pages')->with('flash_notice', 'Pagina verwijderd');
        }
        else {
            return Redirect::route('pages')->with('flash_notice', 'Pagina niet verwijderbaar');
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listpages()
    {
        $pages = Page::all();
        return view('pages.list',compact('pages'));
    }





}

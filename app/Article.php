<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The table the articles are stored in.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * The model name.
     *
     * @var string
     */
    public static $name = 'article';

    /**
     * The properties on the model that are dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The revisionable columns.
     *
     * @var array
     */
    protected $keepRevisionOf = ['title', 'summary', 'body'];

    /**
     * The columns to select when displaying an index.
     *
     * @var array
     */
    public static $index = ['id', 'title', 'summary'];

    /**
     * The max articles per page when displaying a paginated index.
     *
     * @var int
     */
    public static $paginate = 10;

    /**
     * The columns to order by when displaying an index.
     *
     * @var string
     */
    public static $order = 'id';

    /**
     * The direction to order by when displaying an index.
     *
     * @var string
     */
    public static $sort = 'desc';

    /**
     * The article validation rules.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'summary' => 'required',
        'body' => 'required',
        'user_id' => 'required',
        'page' => 'required'
    ];

    public $fillable = [
        'title',
        'body',
        'summary',
        'page',
    ];

    public function getTable()
    {
        return $this->table;
    }

}

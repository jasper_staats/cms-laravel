<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleConnector extends Model
{
    protected $table = 'roleconnector';

    public function getRole() {
        $role = Roles::where('id',$this->role_id)->firstOrFail();
        return $role;
    }
}

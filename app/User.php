<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use SyncableGraphNodeTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'facebook_id', 'avatar','first_name','last_name','access_token','gender' , 'validated'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $name;

    public function getName(){
        $this->name;
    }

    /**
     * Still have to integrate permissions
     * @return bool
     */
    public function isAdmin() {
        $admin = false;
        if(\Auth::check()) {
            $validated = \Auth::user()->getAttribute('validated');

            if($validated) {
                if($this->getFunction() == 'admin') {
                    $admin = true;
                }
            }
        }
        return $admin;
    }

    /**
     * Get role as object
     * @return mixed
     */
    public function getRole(){
        $roleconnector = $this->getRoleConnector();
        $role = $roleconnector->getRole();
        return $role;
    }

    /**
     * Get the string back as function
     * @return mixed
     */
    public function getFunction(){
        $role = $this->getRole();
        return $role->function;
    }

    /**
     * Get the roleconnector that is linked to this user
     * @return mixed
     */
    public function getRoleConnector(){
        return RoleConnector::where('user_id',$this->id)->firstOrFail();
    }


}

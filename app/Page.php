<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

class Page extends Model
{
    protected $table = 'page';

    /**
     * The model name.
     *
     * @var string
     */
    public static $name = 'page';

    /**
     * The properties on the model that are dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The columns to select when displaying an index.
     *
     * @var array
     */
    public static $index = ['id', 'name'];

    /**
     * The max articles per page when displaying a paginated index.
     *
     * @var int
     */
    public static $paginate = 10;

    /**
     * The columns to order by when displaying an index.
     *
     * @var string
     */
    public static $order = 'id';

    /**
     * The direction to order by when displaying an index.
     *
     * @var string
     */
    public static $sort = 'desc';

    /**
     * The article validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name'   => 'required',
    ];

    public $fillable = ['name' , 'url'];

    /**
     * @return string
     */
    public function getTable(){
        return $this->table;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPageID(){
        $current = Route::currentRouteName();
        return $current;
    }

    /**
     * @return mixed
     */
    public function isDeleteable(){
        return $this->deleteable;
    }

    /**
     * @return bool
     */
    public function hasChildren(){
        $children = Page::where('parent',$this->id)->get();
        return (count($children) > 0 ) ? true : false ;
    }

    /**
     * @return bool
     */
    public function isParent() {
        return ($this->getChildren()) ? true : false;
    }

    /**
     * @return bool
     */
    public function isChild(){
        return ($this->parent != 0 ) ? true : false ;
    }

    /**
     * @return null
     */
    public function getChildren(){
        if($this->hasChildren()) {
            $children = Page::where('parent',$this->id)->orderBy('weight','asc')->get();
            return $children;
        }
        else {
            return null;
        }

    }

    /**
     * @return mixed
     */
    public function isAuth(){
        return $this->auth;
    }

    /**
     * @return mixed
     */
    public function isActive() {
        return $this->active;
    }

    /**
     * Get the current auth route if it is auth()
     * @param $method
     */
    public function routeAuth($method) {
        (!$this->auth) ?  Route::get($this->url, ['as' => $this->url, 'uses' => $method]) :  Route::get($this->url, ['middleware' => 'auth', 'as' => $this->url, 'uses' => $method]);
    }

}

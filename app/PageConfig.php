<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pageconfig';

    /**
     * The model name.
     *
     * @var string
     */
    public static $name = 'pageconfig';

    /**
     * The properties on the model that are dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The columns to select when displaying an index.
     *
     * @var array
     */
    public static $index = ['id', 'name'];

    /**
     * The max articles per page when displaying a paginated index.
     *
     * @var int
     */
    public static $paginate = 10;

    /**
     * The columns to order by when displaying an index.
     *
     * @var string
     */
    public static $order = 'id';

    /**
     * The direction to order by when displaying an index.
     *
     * @var string
     */
    public static $sort = 'desc';

    /**
     * The article validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name'   => 'required',
    ];

    public $fillable = ['name' , 'url'];

    public function getTable(){
        return $this->table;
    }

    public function getName(){
        return $this->name;
    }

    public function getPageID(){
        $current = Route::currentRouteName();
        return $current;
    }


}

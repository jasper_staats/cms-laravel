<?php

namespace CMS\Classes\Facades;

use Illuminate\Support\Facades\Facade;

class CMS extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'CMS'; // the IoC binding.
    }
}


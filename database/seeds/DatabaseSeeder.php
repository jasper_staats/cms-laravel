<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->command->warn('--------------');
        $this->command->info('Setup Started');
        $this->call(Setup::class);
        $this->command->info('Setup finished');
        $this->command->warn('--------------');
        Model::reguard();
    }



/**
 * Run the database seeds.
 *
 * @return void
 */



}

class Setup extends Seeder {

    public function run()
    {
        $this->command->info('- Admin added');
        $user = \App\User::create(['name'=>'MEN Technology & Media' ,'email'=>'info@men-webdesign.nl','facebook_id'=>'mentechmedia','avatar'=>'https://graph.facebook.com/v2.4/mentechmedia/picture?width=200','first_name'=>'MEN','last_name'=>'Tech&Media' , 'validated'=>1]);
        $page = \App\Page::create(['name'=>'front','url'=>'home']);
        $this->command->info('- Home page added');
        $admin = \App\Roles::create(['function'=>'admin']);
        $moderator = \App\Roles::create(['function'=>'moderator']);
        $guest = \App\Roles::create(['function'=>'guest']);
        $this->command->info('- Roles added');
        $roleconnector = \App\RoleConnector::create(['user_id'=>$user->id,'role_id'=>$admin->id]);
        $roleconnector = \App\RoleConnector::create(['user_id'=>2,'role_id'=>$admin->id]);
        $article = \App\Article::create(['title'=>'Welkom bij uw eerste pagina!','body'=>'Om uw eerste artikel aan te maken moet u eerst inloggen uw adminpanel','summary' => 'welkom','user_id'=>$user->id,'page'=>$page->id,'is_front'=>1,'published'=>1]);
        $this->command->info('- Test article added');
    }

}

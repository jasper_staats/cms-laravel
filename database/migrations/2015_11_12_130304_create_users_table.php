<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->string('email')->unique();
			$table->string('facebook_id')->unique();
			$table->string('avatar')->nullable();
			$table->rememberToken('remember_token');
			$table->string('gender')->nullable();
			$table->string('first_name')->nullable();
			$table->string('last_name');
			$table->string('access_token')->nullable();
			$table->boolean('validated');
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesTable extends Migration {

	public function up()
	{
		Schema::create('articles', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('summary')->nullable();
			$table->string('body');
			$table->string('title');
			$table->datetime('published_at');
			$table->datetime('deleted_at');
			$table->boolean('revisioned');
			$table->boolean('published');
			$table->integer('user_id')->unsigned();
			$table->integer('page')->unsigned();
			$table->boolean('is_front');
			$table->string('big_image_file')->nullable();
			$table->boolean('big_image');
			$table->string('thumbnail')->nullable();
			$table->integer('permission')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('articles');
	}
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigurationTable extends Migration {

	public function up()
	{
		Schema::create('configuration', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('logo')->nullable();
			$table->string('Title')->nullable();
			$table->integer('theme')->unsigned();
			$table->integer('permission')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('configuration');
	}
}
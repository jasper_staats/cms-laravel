<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageTable extends Migration {

	public function up()
	{
		Schema::create('page', function(Blueprint $table) {
			$table->increments('id');
			$table->string('url')->nullable();
			$table->timestamps();
			$table->string('name')->nullable();
			$table->integer('parent')->unsigned();
			$table->integer('weight')->unsigned();
			$table->string('description')->nullable();
			$table->boolean('active');
			$table->boolean('auth');
			$table->boolean('contact');
			$table->boolean('deleteable');
		});
	}

	public function down()
	{
		Schema::drop('page');
	}
}
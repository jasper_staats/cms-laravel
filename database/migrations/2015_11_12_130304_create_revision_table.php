<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRevisionTable extends Migration {

	public function up()
	{
		Schema::create('revision', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('description')->nullable();
			$table->integer('user_id')->unsigned();
			$table->integer('article_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('revision');
	}
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageconfigTable extends Migration {

	public function up()
	{
		Schema::create('pageconfig', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('page')->unsigned();
			$table->boolean('recent');
			$table->boolean('big_image');
			$table->string('big_image_file')->nullable();
			$table->boolean('footer');
			$table->boolean('special_layout');
			$table->boolean('gallery');
			$table->integer('gallery_id')->nullable();
			$table->integer('theme')->unsigned();
			$table->integer('permission')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('pageconfig');
	}
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateThemeTable extends Migration {

	public function up()
	{
		Schema::create('theme', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('css');
			$table->string('Description');
		});
	}

	public function down()
	{
		Schema::drop('theme');
	}
}
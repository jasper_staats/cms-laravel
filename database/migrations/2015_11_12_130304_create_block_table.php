<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlockTable extends Migration {

	public function up()
	{
		Schema::create('block', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('weight')->nullable();
			$table->integer('page')->unsigned();
			$table->integer('permission')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('block');
	}
}
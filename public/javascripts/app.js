
/**
 * Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/* exported initSample */

var initCKEditor = ( function() {
    return function() {
        var editorElement = CKEDITOR.document.getById( 'editor' );
        // Depending on the wysiwygare plugin availability initialize classic or inline editor.
            CKEDITOR.replace( 'editor' );

    };} )();


<html>
<head>
    <title>{!! \CMS\CMS::getTitle() !!}</title>
    {!! HTML::style('stylesheets/styles.css') !!}
    {!! HTML::style('stylesheets/font-awesome.min.css') !!}
    {!! HTML::style('stylesheets/sweetalert.css') !!}
    <script type="text/javascript" src="../javascripts/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="../javascripts/bootstrap.js"></script>
    <script type="text/javascript" src="{{ URL::asset('javascripts/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('javascripts/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('javascripts/ohsnap.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('javascripts/readmore.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('javascripts/main.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>

</head>
<body>
@include('pages/dashboard/partials/header')
@include('partials/navigation')

<div class="container-fluid {!! \CMS\CMS::getStyleID() !!}">
    <div class="content">
        @include('partials/errors')
        @yield('content')
    </div>
</div>
@include('pages/dashboard/partials/footer')

</body>
</html>

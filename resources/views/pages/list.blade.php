@extends('dashboard')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Pagina's overzicht</div>
        <table class="table table-responsive">
            <thead>
            <th></th>
            <th>Naam</th>
            <th colspan="2">Actie</th>
            </thead>
            <tbody>
            @foreach($pages as $page )
                <tr>
                    <td>{{$page->id}}</td>
                    <td><a href='/pages/show/{{$page->id}}'>{!! $page->name  !!}</a></td>
                    <td><a href='{{URL::to('pages/edit')}}/{{$page->id}}' class="btn btn-primary">Edit</a></td>

                    <td>@if($page->isDeleteable())<a href='{{URL::to('pages/destroy')}}/{{$page->id}}'
                                                     class="btn btn-danger">Delete</a> @endif</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
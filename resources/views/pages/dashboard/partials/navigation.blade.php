<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">


        <div class="media">
            <img class="media-object dp img-circle" src="{!!  Auth::user()->avatar !!}"
                 style="width: 100px;height:100px;">

            <div class="media-body">
                <h4 class="media-heading">{!!  Auth::user()->name !!}
                    {{--<br>
                    <small><strong>{!! Auth::user()->getFunction() !!}</strong></small>--}}
                    <br>
                    <small>{!!  Auth::user()->email !!}</small>
                </h4>
            </div>
        </div>
        <br>
        <li class="sidebar-brand">
            <a href="{!! URL::to('dashboard') !!}">
                Dashboard
            </a>
        </li>
        @include('pages/dashboard/partials/navigation-links')
    </ul>
</div>
<!-- /#sidebar-wrapper -->
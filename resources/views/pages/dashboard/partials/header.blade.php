@if (Auth::check() && Auth::user()->isAdmin())
    <div id="wrapper">
        @include('pages/dashboard/partials/navigation')
        <div class="togglebar"><a href="#menu-toggle" class="btn btn-primary" id="menu-toggle"><p><</p>
            </a>
        </div>
        <!-- Page Content -->
        <div id="page-content-wrapper" class="{!! \CMS\CMS::getStyleID()!!}">
@endif
<li class="{{Active::pattern('*home*')}}"><a href='{{URL::route('home')}}'> Hoofdpagina </a></li>
<li><a class="sidebar-dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        Paginas <span class="caret"></span>
    </a>
    <ul class="sidebar-dropdown-menu">
        <li class="{{Active::pattern('*pages/createnew*')}}"><a href='{{URL::route('pages.createnew')}}'> Nieuw </a></li>
        <li class="{{Active::pattern('*pages')}}"><a href='{{URL::route('pages')}}'> Overzicht </a></li>
    </ul>
</li>

<li><a class="sidebar-dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        Gebruikers <span class="caret"></span>
    </a>
    <ul class="sidebar-dropdown-menu">
        <li class="{{Active::pattern('*users*')}}"><a href='{{URL::route('users')}}'> Overzicht </a></li>
        <li class="{{Active::pattern('*roles*')}}"><a href='{{URL::route('roles')}}'> Rollen </a></li>
    </ul>
</li>
<li class="{{Active::pattern('*articles*')}}"><a href='{{URL::route('articles')}}'> Inhoud </a></li>
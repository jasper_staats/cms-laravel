@extends('dashboard')
@section('content')
    <div id='dashboard-form' class="panel panel-primary">
        <div class="panel-heading">Pagina maken</div>
        <div class="panel-body col-sm-12">
            {!! Form::open(array('route' => array('pages.create') , 'method' => 'post')) !!}
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Algemeen</div>

                    <div class="form-group">
                        {!! Form::label('name', 'Naam :', array('class' => '')) !!}
                        {!! Form::text('name', $page->name, array('class' => 'form-control','placeholder'=>'naam')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('url', 'URL :', array('class' => '')) !!}
                        {!! Form::text('url', $page->url, array('class' => 'form-control','placeholder'=>'plaats hier een url ( home , contact etc.)','id'=>'')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description', 'Omschrijving : ', array('class' => '')) !!}
                        {!! Form::text('description', $page->description, array('class' => 'form-control','placeholder'=>'Omschrijving','id'=>'')) !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Navigatie</div>
                    <div class="input-group">
                        {!! Form::label('parent', 'Plaatsen onder : ', array('class' => '')) !!}
                        <?php
                        $pages = [];
                        foreach (\App\Page::all() as $page) {
                            if ($page->parent == 0) {
                                $pages[$page->id] = $page->name;
                            }
                        }

                        ?>
                        {!! Form::select('parent',$pages, null, array('class' => 'form-control','placeholder'=>'Selecteer','id'=>'')) !!}
                    </div>

                    <div class="input-group">
                        {!! Form::label('weight', 'Gewicht', array('class' => '')) !!}
                        {!! Form::number('weight',$page->weight, array('class' => 'form-control','placeholder'=>'','id'=>'')) !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Opties</div>
                    <div class="form-group">
                        {!! Form::label('active', 'Actief maken', array('class' => '')) !!}
                        {!! Form::checkbox('active', $page->active, array('class' => 'form-control','placeholder'=>'','id'=>'')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('auth', 'Login vereist', array('class' => '')) !!}
                        {!! Form::checkbox('auth',$page->auth, array('class' => 'form-control','placeholder'=>'','id'=>'')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('deleteable', 'Verwijderbaar', array('class' => '')) !!}
                        {!! Form::checkbox('deleteable', $page->deleteable, array('class' => 'form-control','placeholder'=>'','id'=>'')) !!}
                    </div>
                    {!! Form::submit('Submit', array('class' => 'col-sm-12 btn btn-primary')) !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div>

    </div>
@stop
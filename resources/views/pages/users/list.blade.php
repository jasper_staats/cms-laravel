@extends('dashboard')
@section('content')
    <div class="row user-list">
        <div class="col-lg-5">
            @foreach($users as $user)

                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object dp img-circle" src="{{$user->avatar}}"
                             style="width: 100px;height:100px;">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$user->name }}
                            <br>
                            <small> <strong>{{ucfirst($user->function)}}</strong></small><br>
                            <small> {{$user->email}}</small>
                        </h4>
                        <h5><a href="https://www.facebook.com/{{$user->facebook_id}}">Facebook profile</a></h5>
                        <hr style="margin:8px auto">
                        <span class="label label-default">Member since : {{$user->created_at}}</span>

                    </div>
                </div>

            @endforeach
        </div>


    </div>
@stop

@extends('dashboard')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Artikel maken</div>
    {!! Form::open(array('route' => array('articles.create') , 'method' => 'post')) !!}
    <table>
        <tbody>
        <tr>
            <td>{!! Form::label('title', 'Titel', array('class' => '')) !!}</td>
            <td>{!! Form::text('title', '', array('class' => 'form-control','placeholder'=>'')) !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('body', 'Body', array('class' => '')) !!}</td>
            <td>{!! Form::textarea('body', '', array('class' => 'form-control','placeholder'=>'','id'=>'editor')) !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('summary', 'Summary', array('class' => '')) !!}</td>
            <td>{!! Form::textarea('summary', '', array('class' => 'form-control','placeholder'=>'')) !!}</td>
        </tr>
        <tr>
            <td>{!! Form::label('published', 'Publish', array('class' => '')) !!}</td>
            <td>{!! Form::checkbox('published', 1 ,null, array('class' => 'form-control','placeholder'=>'')) !!}</td>
        </tr>
        </tbody>
    </table>
    {!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
    {!! Form::close() !!}
    @include('partials/ckeditor')
    </div>
@stop
@extends('dashboard')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Artikel aanpassen</div>
    {!! Form::model($article, array('route' => array('articles.update', $article->id)))!!}
    {!! Form::label( 'title', 'Title') !!}
    {!! Form::text('title', $article->title , array('class' => 'form-control')) !!}
    {!! Form::label( 'title', 'Body') !!}
    {!! Form::textarea('body', $article->body , array('class' => 'form-control' , 'id'=>'editor' )) !!}
    {!! Form::label( 'summary', 'Samenvatting') !!}
    {!! Form::textarea('summary', $article->summary , array('class' => 'form-control')) !!}
    {!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
    {!! Form::close()!!}
    @include('partials/ckeditor')
    </div>
@stop
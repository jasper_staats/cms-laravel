@extends('dashboard')
@section('content')
    @include('pages/articles/partials/navigation')
    <div class="panel panel-default">
        <div class="panel-heading">Artikelen overzicht</div>
    <table class="table table-responsive">
        <thead>
        <th></th>
        <th>Titel</th>
        <th colspan="2">Actie</th>
        </thead>
        <tbody>
        @foreach($articles as $article )
            <tr>
                <td>{{$article->id}}</td>
                <td><a href='/articles/show/{{$article->id}}'>{!! $article->title  !!}</a></td>
                <td><a href='{{URL::to('articles/edit')}}/{{$article->id}}' class="btn btn-primary">Edit</a></td>
                <td><a href='{{URL::to('articles/destroy')}}/{{$article->id}}' class="btn btn-primary">Delete</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@stop
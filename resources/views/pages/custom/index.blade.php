@extends('app')
@section('content')
    <h1 class="page-title">{{$page_type}}</h1>
    <div class="col-sm-8 col-md-9">
        @if(!count($articles) > 0)
            <h3 class="well
            ">Geen artikelen geplaatst</h3>
        @endif
        @foreach($articles as $article)
            @if($article->published)
                <div class="article-item">
                    {{-- Enable edit button if user has admin permissions--}}
                    @if (Auth::check() && Auth::user()->isAdmin())
                        <a href="{{URL::to('articles/edit/'.$article['id'].'')}}" class="edit-button">Edit</a>
                    @endif
                    {{-- End edit button--}}
                    <a href="{{URL::to('articles/show/'.$article['id'].'')}}">
                        <div class="title"><h1>{{ $article['title'] }}</h1>
                            <span>Geschreven door : {{ \App\User::findOrFail($article['user_id'])->name }}</span></div>
                    </a>
                    <hr>
                    {!! $article['body']!!}

                </div>
                <hr>
            @endif
        @endforeach
    </div>
    {{--
    Get recent articles if enabled in PageConfig and if there is any content
    --}}
    @if(count(\App\Http\Controllers\ArticleController::getRecent('ASC',strtolower($page_url))) > 0)
        <div class="col-sm-4 hidden-xs col-md-3 recent {{$page_type}}">
            <div class="title">
                <h1 class="text-center"> Recent
                    <h5 class="text-center">@if($page_type != 'Home') @ <strong>{{ $page_type }}@else
                                Nieuws @endif</strong></h5>
                </h1>


                <div class="content">
                    {{--   For each article that is related to the page_type (front, home , news etc. )   --}}
                    <ul>
                        @foreach(\App\Http\Controllers\ArticleController::getRecent('ASC',strtolower($page_url)) as $recent )
                            <li><a href="{{URL::to('articles/show/'.$recent['id'].'')}}"> {{ $recent['title'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    {{--
    End recent
    --}}
@stop
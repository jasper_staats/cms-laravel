
@extends('app')

@section('content')
    <div class = "container">
        <div class="wrapper">
            <form action="" method="post" name="Login_Form" class="form-signin">
                <h3 class="form-signin-heading">Welcome Back! Please Sign In</h3>
                <hr class="colorgraph"><br>
                <a class="btn btn-lg btn-primary btn-block" href="../auth/facebook" role="button"><i class="fa fa-facebook-f"></i> - Login with  Facebook</a>
            </form>
        </div>
    </div>
@stop
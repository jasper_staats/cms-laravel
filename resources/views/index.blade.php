@extends('app')
@section('content')
    <?php $page_type = 'front'; ?>
    <div class="col-sm-9">
        @foreach($articles as $article)
            <div class="article-item">
                @if (Auth::check() && Auth::user()->isAdmin())
                    <a href="{{URL::to('articles/edit/'.$article['id'].'')}}" class="edit-button">Edit</a>
                @endif
                <a href="{{URL::to('articles/show/'.$article['id'].'')}}">
                    <div class="title"><h1>{{ $article['title'] }}</h1>
                        <span>Geschreven door : {{ \App\User::findOrFail($article['user_id'])->name }}</span></div>
                </a>
                <hr>
                {!! $article['body']!!}

            </div>
            <hr>


        @endforeach
    </div>
    <div class="col-sm-3 recent {{$page_type}}">
        <?php
        $page = \App\Page::where('url', $page_type)->get();
        foreach ($page as $p) {
            $pageName = $p['name'];
        }
        ?>
        <h1> Recent </h1>
        <hr>
        <div class="content">
            <ul>
                @foreach(\App\Http\Controllers\ArticleController::getRecent('ASC',$page_type) as $recent )
                    <li><a href="{{URL::to('articles/show/'.$recent['id'].'')}}"> {{ $recent['title'] }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
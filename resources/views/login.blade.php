@if (Auth::check())
    <li><a class="login-nav"  href="logout" role="button"><i class="fa fa-exclamation-triangle"></i> Logout</a></li>
@else
    <li><a class='login-nav' href='{{URL::route('auth/facebook')}}' role="button"><i class="fa fa-facebook-f"></i> Login with Facebook</a></li>
@endif
<div id="ohsnap">
    @if (Session::has('snap_error'))
        <script>
            ohSnap('{{ Session::pull('snap_error') }}', 'red');
        </script>
    @endif
    @if(Session::has('snap_notice'))
        <script>
            ohSnap('{{ Session::pull('snap_notice') }}', 'black');
        </script>

    @endif
    @if(Session::has('snap_success'))
        <script>
            ohSnap('{{ Session::pull('snap_success') }}', 'blue');
        </script>

    @endif
</div>

@if (Session::has('flash_error')  )
    <script>
        swal({
            title: "",
            text: "{{ Session::pull('flash_error') }}",
            type: "error",
            timer: 2000
        });
    </script>
@endif
@if (Session::has('flash_notice')  )
    <script>
        swal({
            title: "",
            text: "{{ Session::pull('flash_notice') }}",
            type: "info",
            timer: 2000
        });
    </script>
@endif
@if (Session::has('flash_success') )
    <script>
        swal({
            title: "",
            text: "{{Session::pull('flash_success') }}",
            type: "success",
            timer: 2000
        });
    </script>
@endif
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @include('partials/logo')
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @yield('navigation-left')
                @include('partials/navigation-links')
                @yield('navigation_links')
                @yield('navigation')
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @yield('navigation-right') @include('login')
            </ul>
        </div>
    </div>
</nav>

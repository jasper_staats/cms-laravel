@foreach(\App\Http\Controllers\PagesController::getNavigationParent() as $url)
    @if(!$url->isParent() && !$url->hasChildren())
    <li class="parent {{Active::pattern('*'.$url->url.'*')}}"><a href='{{URL::route(''.$url->url.'')}}'> {{$url->name}}</a></li>
    @endif

    @if($url->hasChildren() && $url->isParent())
{{--        <li class="{{Active::pattern('*'.$url->url.'*')}}"><a href='{{URL::route(''.$url->url.'')}}'> {{$url->name}}</a></li>--}}
<li class="parent {{Active::pattern('*'.$url->url.'*')}}"><a class="dropdown-toggle" data-toggle="dropdown" href='{{URL::route(''.$url->url.'')}}' role="button" aria-haspopup="true" aria-expanded="false">

        {{$url->name}} <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="child {{Active::pattern('*'.$url->url.'')}}"><a href='{{URL::route(''.$url->url.'')}}'> {{$url->name}}</a></li>
        @foreach($url->getChildren() as $page )
            <li class="child {{Active::pattern('*'.$page->url.'*')}}"><a href='{{URL::route(''.$page->url.'')}}'> {{$page->name}}</a></li>
        @endforeach
    </ul>
</li>
    @endif

@endforeach

